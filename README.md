# Serenity Cucumber project
## About
This is a test project which goal is to present REST Api testing using Java, Cucumber and Serenity.

Resource under test: https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product}

## Get Product Details

This API endpoint retrieves the details of a product based on the provided product name.
### Available products:
    "apple", "pasta", "orange" and "cola"
### Example
**HTTP Method:** GET

**Request:**
/search/demo/orange

**Response:**
- Status Code: 200 (OK)
- Body: JSON object containing the product details, including properties like `provider`, `title`, `price`, `unit` and `url`.

## Prerequisites
- Java jdk 11.0.11 - Make sure you have Java Development Kit (JDK) version 11.0.11 or higher installed on your system. You can download it from [Oracle's website](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html).
- apache-maven-3.8.1 - The project uses Apache Maven as the build and dependency management tool. Make sure you have Apache Maven version 3.8.1 or higher installed. You can download it from the [official Apache Maven website](https://maven.apache.org/download.cgi).

## Getting Started

To get started with the project, follow these steps:

1. Clone the repository:

   ```shell
   git clone https://gitlab.com/s.martonosi/leaseplan-test-assignment.git

2. Install the required dependencies using Maven:
    ```shell
   cd project-name
    mvn clean install

3. Run the project:
    ```shell
   clean verify serenity:aggregate

4. Run test by tag:
    ```shell
   clean verify serenity:aggregate -Dcucumber.filter.tags=@TEST01
The test results will be recorded in the target/site/serenity directory.
## The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
    + java
      + starter
  + test
    + java  
      + starter
        + apiservice            Directory that contains classes related to API services
        + stepdefinitions       Directory that contains the step definition classes for Cucumber scenarios
    + resources  
      + feature.search          Feature files directory
        + GetProducts.feature   Feature containing BDD scenarios
     
```


## The sample scenario
In this scenario I search for "orange" and validate the result obtained according to that, first I call the endpoint, then I should get statuscode 200 and I get the result for "orange".
```Gherkin
  Scenario: Searching for "orange" products in product store
    When I call api service to retrieve orange products
    Then I should receive status code 200
    And I should see results displayed for orange products
```
A scenario can also be created using the Scenario Outline, in which case this would be an example of such a scenario:
```Gherkin
  Scenario Outline: Searching for products in product store
    When I call api service to retrieve <products> products
    Then I should receive status code 200
    And  I should see results displayed for <products> products
      Examples:
      | products |
      | apple    |
      | orange   |
      | pasta    |
      | cola     |
```
## In order to improve the project:

- Remove Gradle settings and switch to Maven for dependency management.
- Refactor the code structure to enhance maintainability.
- Implement a comprehensive test suite with various test scenarios to achieve better test coverage.

## What was refactored and why?
- I changed the name of the step class to "ProductsAPI" since the application is not related to Cars.
```java
    @Steps
    public CarsAPI carsAPI;  
```
```java
    @Steps
    public ProductsAPI ProductsAPI;  
```
- I changed the method names that were written in third person to first person narration.
- The name of the argument has also been changed in order to know what it is about.
```java
 @When("he calls endpoint {string}")                
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }
```
```java
    @When("I call the endpoint {string}")
    public void iCallEndpoint(String fullPath) {
        productsAPI.get(fullPath);
    }
```

- There was a typo here.
- The convention was also changed to camelcase.
```java
    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() {
        restAssuredThat(response -> response.body("error", contains("True")));
    }
```
- There were several bugs in the post_products.feature file that have been fixed. I changed the names of steps that were written in third person to first person narration. We can also see in the path that in one example "orange" is fetched, and the display of results for "apple" is expected, such a problem also exists in the case where the endpoint for fetching "apple" is called, and the display of results for "mango" is expected. In the last When step, an attempt is made to get "car" which is not in the list of available products. There is a typo in the last step.

```gherkin
  Scenario:
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/orange"
    Then he sees the results displayed for apple
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/apple"
    Then he sees the results displayed for mango
    When he calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/car"
    Then he doesn not see the results
```
- The post_product.feature file is renamed to GetProducts.feature. In this way and in this place, the camelcase convention is also used and it is also indicated that it is about Get products, not adding new ones.

- I changed the structure of the project by adding a new package "apiservices" to test/java/starter which contains the class "ProductsAPI"
