Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios
  @TEST01
  Scenario: Searching for "orange" products in product store
    When I call api service to retrieve orange products
    Then I should receive status code 200
    And I should see results displayed for orange products

  @TEST02
  Scenario: Searching for "apple" products in product store
    When I call api service to retrieve apple products
    Then I should receive status code 200
    And I should see results displayed for apple products

  @TEST03
  Scenario: Searching for "pasta" products in product store
    When I call api service to retrieve pasta products
    Then I should receive status code 200
    And I should see results displayed for pasta products

  @TEST04
  Scenario: Searching for "cola" products in product store
    When I call api service to retrieve cola products
    Then I should receive status code 200
    And I should see results displayed for cola products

#All this positive test scenarios can be created as one Scenario Outline
  @TEST05
  Scenario Outline: Searching for products in product store
    When I call api service to retrieve <products> products
    Then I should receive status code 200
    And  I should see results displayed for <products> products
    Examples:
      | products |
      | apple    |
      | orange   |
      | pasta    |
      | cola     |

  @TEST06
  Scenario: Negative test scenario - Search not available product
    When I call api service to retrieve spacestation products
    Then I should receive status code 404
    And  I should get appropriate product not found message




