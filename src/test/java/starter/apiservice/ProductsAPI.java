package starter.apiservice;

import io.restassured.response.ValidatableResponse;
import net.thucydides.core.annotations.Step;
import net.serenitybdd.rest.SerenityRest;

public class ProductsAPI {

    private static final String BASE_URL = "https://waarkoop-server.herokuapp.com/api/v1/search/demo";

    @Step("Get request")
    public ValidatableResponse get(String endpoint) {
        ValidatableResponse response = SerenityRest.given()
                .baseUri(BASE_URL)
                .log().all()
                .contentType("application/json")
                .when()
                .get(endpoint)
                .then().log().all();
        return response;
    }
}