package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;
import starter.apiservice.ProductsAPI;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class SearchStepDefinitions {
    @Steps
    public ProductsAPI productsAPI;

    @When("I call api service to retrieve {word} products")
    public void iCallApiServiceToProducts(String product) {
        productsAPI.get(product);
    }

    @Then("I should see results displayed for {word} products")
    public void iSeeTheResultsDisplayedFor(String product) {
        restAssuredThat(response -> response.body("any { it.containsKey('title') && it.title.contains('" + product + "')}", Matchers.is(true)));
    }

    @Then("I should receive status code {int}")
    public void iSeeStatusCodeInResponse(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @Then("I should get appropriate product not found message")
    public void iShouldGetAppropriateMessage() {
        restAssuredThat(response -> response.body("detail.message", Matchers.containsString("Not found")));
        restAssuredThat(response -> response.body("detail.error", Matchers.is(true)));
    }
}